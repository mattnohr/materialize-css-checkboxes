# Materialize CSS Checkboxes

Extension for Materialize CSS to have filled-in checkboxes that match the color themes.

I was using [MaterializeCSS](http://materializecss.com/) and wanted to have checkboxes that were filled in but matched the supported [color themes](http://materializecss.com/color.html). This is not supported out of the box. Looking at [this StackOverflow](https://stackoverflow.com/questions/35261021/change-color-of-checkbox-in-materialize-framework) question laid out the basics, but did not have every color. I wrote a script to generate the full CSS file with all the colors and this is the result.

There are **256 colors** that are supported.

## Usage

First, add the `checkbox-colors.css` file to your project.

Use code like this for your checkbox:

```
<input type="checkbox" class="filled-in checkbox-orange" id="filled-in-box" checked="checked" />
<label for="filled-in-box"></label>
```

The class options are like:
* `filled-in checkbox-orange`
* `filled-in checkbox-orange-lighten-1`
* `filled-in checkbox-orange-darken-4`
* `filled-in checkbox-orange-accent-2`
